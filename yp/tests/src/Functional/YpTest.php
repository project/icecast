<?php

namespace Drupal\Tests\yp\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for YP module.
 *
 * @group icecast
 */
class YpTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['yp'];

  /**
   * Functional tests for yp.
   */
  public function testYp(): void {
  }

}
