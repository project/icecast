<?php

namespace Drupal\yp;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an Icecast YP stream entity type.
 */
interface YpStreamInterface extends ContentEntityInterface, EntityChangedInterface {

}
