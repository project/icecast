<?php

namespace Drupal\yp\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Controller\ControllerBase;
use Drupal\yp\Entity\YpStream;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Default controller for the yp module.
 */
class YpController extends ControllerBase {

  /**
   * Stream fields.
   *
   * @var string[]
   */
  protected $fields;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Response object.
   *
   * @var \Symfony\Component\HttpFoundation\Response
   */
  protected $response;

  /**
   * Stream ID.
   *
   * @var int|null
   */
  protected $sid;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  final public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('datetime.time'));
  }

  /**
   * Endpoint for YP CGI requests.
   */
  public function cgi(Request $request): Response {
    $action = is_string($request->get('action')) ? trim($request->get('action')) : '';
    if (!in_array($action, ['add', 'remove', 'touch'])) {
      throw new AccessDeniedHttpException();
    }
    $this->response = new Response();
    $this->fields['last_touch'] = (string) $this->time->getRequestTime();
    $this->sid = is_string($request->get('sid')) ? (int) $request->get('sid') : NULL;
    $this->request = $request;
    $this->$action();
    if ($this->config('yp.settings')->get('debug')) {
      $this->getLogger('yp')->debug('YP %action: %query %request %response', [
        '%action' => $action,
        '%query' => print_r($request->query->all(), TRUE),
        '%request' => print_r($request->request->all(), TRUE),
        '%response' => print_r($this->response->headers->all(), TRUE),
      ]);
    }
    return $this->response;
  }

  /**
   * Builds a mapping from request parameters to database columns.
   *
   * @param string[][] $map
   *   Field aliases.
   */
  public function buildFields(array $map): void {
    foreach ($map as $key => $variables) {
      foreach ($variables as $variable) {
        if (is_string($value = $this->request->get($variable))) {
          $this->fields[$key] = Unicode::validateUtf8($value) ? trim($value) : '';
          // Ensure URL has scheme.
          if ($key === 'url' && $this->fields[$key]) {
            if (!parse_url($this->fields[$key], PHP_URL_SCHEME)) {
              $this->fields[$key] = 'https://' . $this->fields[$key];
            }
          }
        }
      }
    }
  }

  /**
   * Adds a new stream.
   */
  public function add(): void {
    $this->buildFields([
      'server_name' => ['sn'],
      'server_type' => ['type'],
      'genre' => ['genre'],
      'bitrate' => ['audio_bitrate', 'b', 'bitrate', 'ice-bitrate'],
      'samplerate' => ['audio_samplerate', 'samplerate', 'ice-samplerate'],
      'channels' => ['audio_channels', 'channels', 'ice-channels'],
      'listen_url' => ['listenurl'],
      'description' => ['desc'],
      'url' => ['url'],
      'cluster_password' => ['cpswd'],
    ]);
    $this->fields['listing_ip'] = $this->request->getClientIP() ?? '';
    $ypStream = YpStream::create($this->fields);
    $ypStream->save();
    $this->sid = is_string($ypStream->id()) ? (int) $ypStream->id() : $ypStream->id();
    $this->response->headers->set('SID', (string) $this->sid);
    $this->response->headers->set('TouchFreq', '200');
    $this->response->headers->set('YPMessage', $this->sid ? 'Added' : 'Error');
    $this->response->headers->set('YPResponse', $this->sid ? '1' : '0');
  }

  /**
   * Touches (updates) a stream.
   */
  public function touch(): void {
    $this->buildFields([
      'listeners' => ['listeners'],
      'max_listeners' => ['max_listeners'],
      'server_subtype' => ['stype'],
      'current_song' => ['st'],
    ]);
    if ($ypStream = YpStream::load($this->sid)) {
      foreach ($this->fields as $key => $value) {
        $ypStream->set($key, $value);
      }
      $ypStream->save();
    }
    $this->response->headers->set('YPMessage', $ypStream ? 'Touched' : 'SID not found');
    $this->response->headers->set('YPResponse', $ypStream ? '1' : '0');
  }

  /**
   * Removes a stream.
   */
  public function remove(): void {
    if ($ypStream = YpStream::load($this->sid)) {
      $ypStream->delete();
    }
    $this->response->headers->set('YPMessage', 'Removed');
    $this->response->headers->set('YPResponse', '1');
  }

}
