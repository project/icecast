<?php

namespace Drupal\yp;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the YP Stream entity type.
 */
class YpStreamListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new YpStreamListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  final public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Render array.
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total Icecast YP streams received: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Entity IDs.
   */
  protected function getEntityIds() {
    $header = $this->buildHeader();
    $query = $this->getStorage()
      ->getQuery()
      ->accessCheck(TRUE)
      ->tableSort($header);
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Table header.
   */
  public function buildHeader() {
    $header['sid'] = $this->t('Stream ID');
    $header['listen_url'] = [
      'data' => $this->t('Listen URL'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['current_song'] = [
      'data' => $this->t('Current song'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['last_touch'] = [
      'data' => $this->t('Last touch'),
      'field' => 'last_touch',
      'specifier' => 'last_touch',
      'sort' => 'desc',
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header += parent::buildHeader();
    $header['operations'] = ['data' => $header['operations']];
    $header['operations']['class'] = [RESPONSIVE_PRIORITY_LOW];
    return $header;
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Table row.
   */
  public function buildRow(EntityInterface $entity) {
    /** @var YpStreamInterface $entity */
    $row['sid'] = $entity->label();
    $row['listen_url'] = $entity->get('listen_url')->value;
    $row['current_song'] = $entity->get('current_song')->value;
    $last_touch = $entity->get('last_touch')->value;
    $row['last_touch'] = is_numeric($last_touch) ? $this->dateFormatter->format((int) $last_touch) : '';
    return $row + parent::buildRow($entity);
  }

}
