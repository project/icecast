<?php

namespace Drupal\yp\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\UriLinkFormatter;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'yp_listen_link' formatter.
 *
 * @FieldFormatter(
 *   id = "yp_listen_link",
 *   label = @Translation("Link to listen URL with .m3u appended"),
 *   field_types = {
 *     "uri",
 *   }
 * )
 */
class ListenLinkFormatter extends UriLinkFormatter {

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Renderable link(s).
   *
   * @phpstan-ignore missingType.generics,missingType.iterableValue
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->value)) {
        $value = $item->value . '.m3u';
        $elements[$delta] = [
          '#type' => 'link',
          '#url' => Url::fromUri($value),
          '#title' => $value,
        ];
      }
    }

    return $elements;
  }

}
