<?php

namespace Drupal\yp\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Creates YP stream views.
 *
 * @ViewsWizard(
 *   id = "yp_stream",
 *   base_table = "yp_stream",
 *   title = @Translation("YP stream directory")
 * )
 */
class YpStream extends WizardPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $createdColumn = 'last_touch';

}
