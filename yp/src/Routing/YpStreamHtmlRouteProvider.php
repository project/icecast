<?php

namespace Drupal\yp\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class YpStreamHtmlRouteProvider extends AdminHtmlRouteProvider {

}
