<?php

namespace Drupal\yp;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the YP Stream schema handler.
 */
class YpStreamStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   The schema definition for the table.
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();
    if ($table_name === 'yp_stream' && $field_name === 'last_touch') {
      $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
    }
    if ($table_name === 'yp_stream' && $field_name === 'listen_url') {
      $this->addSharedTableFieldIndex($storage_definition, $schema, FALSE);
    }
    return $schema;
  }

}
