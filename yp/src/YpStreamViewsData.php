<?php

namespace Drupal\yp;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the YP Stream entity type.
 */
class YpStreamViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Views data in the format of hook_views_data().
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    $data['yp_stream']['table']['wizard_id'] = 'yp_stream';
    return $data;
  }

}
