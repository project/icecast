<?php

namespace Drupal\yp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides YP settings.
 */
class YpAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yp_admin_settings';
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('yp.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Editable config names.
   */
  protected function getEditableConfigNames(): array {
    return ['yp.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed[]
   *   Form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['debug'] = [
      '#type' => 'radios',
      '#title' => $this->t('CGI debug logging'),
      '#default_value' => $this->config('yp.settings')->get('debug'),
      '#options' => [
        $this->t('Disabled'),
        $this->t('Enabled'),
      ],
      '#description' => $this->t('Log additional debugging information to watchdog.'),
    ];
    return parent::buildForm($form, $form_state);
  }

}
