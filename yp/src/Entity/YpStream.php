<?php

namespace Drupal\yp\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\yp\YpStreamInterface;

/**
 * Defines the YP stream entity class.
 *
 * @ContentEntityType(
 *   admin_permission = "administer icecast yp",
 *   base_table = "yp_stream",
 *   entity_keys = {
 *     "id" = "sid",
 *     "label" = "server_name",
 *   },
 *   handlers = {
 *     "access" = "Drupal\yp\YpStreamAccessControlHandler",
 *     "form" = {
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "list_builder" = "Drupal\yp\YpStreamListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\yp\Routing\YpStreamHtmlRouteProvider",
 *     },
 *     "storage_schema" = "Drupal\yp\YpStreamStorageSchema",
 *     "views_data" = "Drupal\yp\YpStreamViewsData",
 *   },
 *   id = "yp_stream",
 *   label = @Translation("Icecast YP stream"),
 *   label_collection = @Translation("Icecast YP streams"),
 *   label_count = @PluralTranslation(
 *     plural = "@count Icecast YP streams",
 *     singular = "@count Icecast YP stream",
 *   ),
 *   label_plural = @Translation("Icecast YP streams"),
 *   label_singular = @Translation("Icecast YP stream"),
 *   links = {
 *     "collection" = "/admin/reports/yp",
 *     "delete-form" = "/admin/reports/yp/{yp_stream}/delete",
 *     "delete-multiple-form" = "/admin/reports/yp/delete",
 *   },
 *   persistent_cache = FALSE,
 *   render_cache = FALSE,
 *   static_cache = FALSE,
 * )
 */
class YpStream extends ContentEntityBase implements YpStreamInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle(): ?string {
    $title = $this->get('server_name')->value;
    return \is_string($title) ? $title : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(?string $title): self {
    $this->set('server_name', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['parent_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Parent stream ID'));

    $fields['server_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stream name'))
      ->setSetting('max_length', 255);

    $fields['listing_ip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source IP address'))
      ->setSetting('max_length', 39);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stream description'))
      ->setSetting('max_length', 511);

    $fields['genre'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stream genre'))
      ->setSetting('max_length', 255);

    $fields['cluster_password'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cluster password'))
      ->setSetting('max_length', 50);

    $fields['url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Stream URL'))
      ->setSetting('max_length', 255);

    $fields['current_song'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Current song'))
      ->setSetting('max_length', 767);

    $fields['listen_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Listen URL'))
      ->setSetting('max_length', 255);

    $fields['server_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stream type'))
      ->setSetting('max_length', 25);

    $fields['server_subtype'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stream subtype'))
      ->setSetting('max_length', 255);

    $fields['bitrate'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Bit rate'));

    $fields['listeners'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Listeners'));

    $fields['max_listeners'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Max listeners'));

    $fields['channels'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Channels'))
      ->setSetting('max_length', 25);

    $fields['samplerate'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Sample rate'))
      ->setSetting('max_length', 25);

    $fields['last_touch'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last touch'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime(): ?int {
    $last_touch = $this->get('last_touch')->value;
    return is_numeric($last_touch) ? (int) $last_touch : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp): self {
    $this->set('last_touch', $timestamp);
    return $this;
  }

}
