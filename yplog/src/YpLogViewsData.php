<?php

namespace Drupal\yplog;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the YP Log entity type.
 */
class YpLogViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   Views data in the format of hook_views_data().
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    $data['yp_log']['table']['wizard_id'] = 'yp_log';
    return $data;
  }

}
