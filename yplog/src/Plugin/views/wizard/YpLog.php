<?php

namespace Drupal\yplog\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Creates YP log views.
 *
 * @ViewsWizard(
 *   id = "yp_log",
 *   base_table = "yp_log",
 *   title = @Translation("YP logs")
 * )
 */
class YpLog extends WizardPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $createdColumn = 'timestamp';

}
