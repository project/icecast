<?php

namespace Drupal\yplog;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an Icecast YP log entity type.
 */
interface YpLogInterface extends ContentEntityInterface {

}
