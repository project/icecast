<?php

namespace Drupal\yplog\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides YP settings.
 */
class YpLogAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yplog_admin_settings';
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('yplog.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Editable config names.
   */
  protected function getEditableConfigNames(): array {
    return ['yplog.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed[]
   *   Form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['ttl'] = [
      '#type' => 'number',
      '#title' => $this->t('TTL'),
      '#default_value' => $this->config('yplog.settings')->get('ttl'),
      '#description' => $this->t('YP log data time-to-live (TTL) in seconds.'),
      '#min' => 0,
    ];
    return parent::buildForm($form, $form_state);
  }

}
