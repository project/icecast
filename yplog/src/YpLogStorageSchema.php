<?php

namespace Drupal\yplog;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the YP Log schema handler.
 */
class YpLogStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   The schema definition for the table.
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();
    if ($table_name === 'yp_log') {
      if ($field_name === 'listen_url') {
        $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
      }
      if ($field_name === 'timestamp') {
        $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
        $schema['unique keys']['url_time'] = [
          'listen_url',
          'timestamp',
        ];
      }
    }
    return $schema;
  }

}
