<?php

namespace Drupal\yplog\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class YpLogHtmlRouteProvider extends AdminHtmlRouteProvider {

}
