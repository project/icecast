<?php

namespace Drupal\yplog\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\yplog\YpLogInterface;

/**
 * Defines the YP log entity class.
 *
 * @ContentEntityType(
 *   admin_permission = "administer icecast yp",
 *   base_table = "yp_log",
 *   entity_keys = {
 *     "id" = "lid",
 *     "label" = "listen_url",
 *   },
 *   handlers = {
 *     "access" = "Drupal\yplog\YpLogAccessControlHandler",
 *     "form" = {
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "list_builder" = "Drupal\yplog\YpLogListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\yplog\Routing\YpLogHtmlRouteProvider",
 *     },
 *     "storage_schema" = "Drupal\yplog\YpLogStorageSchema",
 *     "views_data" = "Drupal\yplog\YpLogViewsData",
 *   },
 *   id = "yp_log",
 *   label = @Translation("Icecast YP log"),
 *   label_collection = @Translation("Icecast YP logs"),
 *   label_count = @PluralTranslation(
 *     plural = "@count Icecast YP logs",
 *     singular = "@count Icecast YP log",
 *   ),
 *   label_plural = @Translation("Icecast YP logs"),
 *   label_singular = @Translation("Icecast YP log"),
 *   links = {
 *     "collection" = "/admin/reports/yp/log",
 *     "delete-form" = "/admin/reports/yp/log/{yp_log}/delete",
 *     "delete-multiple-form" = "/admin/reports/yp/log/delete",
 *   },
 *   persistent_cache = FALSE,
 *   render_cache = FALSE,
 *   static_cache = FALSE,
 * )
 */
class YpLog extends ContentEntityBase implements YpLogInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['listen_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Listen URL'))
      ->setSetting('max_length', 255);

    $fields['timestamp'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Timestamp'));

    $fields['listeners'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Listeners'));

    return $fields;
  }

}
