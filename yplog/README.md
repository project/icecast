# Icecast YP Log

The Icecast YP log module logs snapshots of the YP directory stream
listener counts on each cron run. These snapshots are stored in the
database for later analysis and graphing.

The logged data can be visualized - e.g. graphing the listener count
over time - using other contributed modules or custom code. A sample
view included with the module requires [Charts
module](https://www.drupal.org/project/charts) and Highcharts library.
