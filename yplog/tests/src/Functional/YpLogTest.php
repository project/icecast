<?php

namespace Drupal\Tests\yplog\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for YP Log module.
 *
 * @group icecast
 */
class YpLogTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['yplog'];

  /**
   * Functional tests for yplog.
   */
  public function testYpLog(): void {
  }

}
